package it.unibo.oop.lab.codeanalysis;

import java.util.ArrayList;
import java.util.Collection;

/*
 * Checkstyle complains: the author and version Javadoc tags should not be used.
 */
/**
 * This code triggers static code analyzers. You should use it to see how things
 * SHOULD NOT be done.
 * @version 1.2
 *
 */
public final class ThisIsHowThingsShouldNotBeDone {

    static final int MAX_NUMBER = 43;

    private ThisIsHowThingsShouldNotBeDone() { };

    /**
     * @param a
     * String that contains arguments
     */
    public static void main(final String[] a) {

        Collection<Object> c = new ArrayList<>();

        for (int i = 0; i < MAX_NUMBER; i++) {
            c.add(new Object());
        }
        c.clear();
    }
}
