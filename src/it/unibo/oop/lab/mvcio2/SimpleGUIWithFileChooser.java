package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import it.unibo.oop.lab.mvcio.Controller;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {

    /*
     * : Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */
    private final JFrame frame = new JFrame("Simple GUI With File Chooser");
    /**
     * builds a new {@link SimpleGUI}.
     */
    private SimpleGUIWithFileChooser(final Controller controller) {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        final JTextArea text = new JTextArea();
        final JPanel canvas = new JPanel();
        final JPanel upperPanel = new JPanel();
        final JButton save = new JButton("Save");
        final JTextField filepath = new JTextField(controller.getPath());
        filepath.setEditable(false);
        final JButton chooseFile = new JButton("Browse...");

        final LayoutManager mgr = new BorderLayout();
        upperPanel.setLayout(new BorderLayout());
        canvas.setLayout(mgr);

        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                try {
                    controller.writeOnFile(text.getText());
                } catch (IOException exc) {
                        JOptionPane.showMessageDialog(null, exc.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        chooseFile.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                final JFileChooser jfc = new JFileChooser("Choose where to save the file...");
                jfc.setSelectedFile(controller.getCurrentFile());
                final int result = jfc.showSaveDialog(frame);
                switch (result) {
                case JFileChooser.APPROVE_OPTION:
                   final File newDestination = jfc.getSelectedFile();
                   controller.setCurrentFile(newDestination);
                   filepath.setText(newDestination.getPath());
                   break;
                case JFileChooser.CANCEL_OPTION:
                    break;
                default:
                    JOptionPane.showMessageDialog(frame, result, "May you never forget me, PunPun", JOptionPane.ERROR_MESSAGE);
                }
            }

        });
        canvas.add(text, BorderLayout.CENTER);
        canvas.add(save, BorderLayout.SOUTH);
        upperPanel.add(filepath, BorderLayout.CENTER);
        upperPanel.add(chooseFile, BorderLayout.LINE_END);
        canvas.add(upperPanel, BorderLayout.NORTH);
        frame.setContentPane(canvas);

        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        frame.setLocationByPlatform(true);
    }

    /**
     * Sets frame visibility to True.
     */
    private void display() {
        frame.setVisible(true);
    }

    /**
     * @param args 
     * 
     * Launches the whole application.
     */
    public static void main(final String... args) {
        final SimpleGUIWithFileChooser gui = new SimpleGUIWithFileChooser(new Controller());
        gui.display();

    }



}
