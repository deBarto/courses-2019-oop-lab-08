package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class SimpleController implements Controller {

    private final List<String> stringHistory = new ArrayList<>();
    private String nextString;

    @Override
    public void setNextStringToPrint(final String str) {
        this.nextString = Objects.requireNonNull(str, "Null values can't be accepted.");
    }

    @Override
    public String getNextStringToPrint() {
        return this.nextString;
    }

    @Override
    public List<String> getPrintedStringHistory() {
        return stringHistory;
    }

    @Override
    public void printCurrentString() throws IllegalStateException {
        if (this.nextString == null) {
            throw new IllegalStateException("There is no string to print.");
        }
        System.out.println(this.nextString);
        stringHistory.add(this.nextString);
    }

}
