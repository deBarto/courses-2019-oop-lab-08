package it.unibo.oop.lab.mvc;

import java.util.List;

/**
 * A controller that prints strings and has memory of the strings it printed.
 */
public interface Controller {
    /**
     * @param str next string to print.
     */
    void setNextStringToPrint(String str);
    /**
     * @return the next string to print.
     */
    String getNextStringToPrint();
    /**
     * @return the list of the printed strings. 
     */
    List<String> getPrintedStringHistory();
    /**
     * Prints the current string.
     * 
     * @throws IllegalStateException in case the current string is unset.
     */
    void printCurrentString() throws IllegalStateException;
}
