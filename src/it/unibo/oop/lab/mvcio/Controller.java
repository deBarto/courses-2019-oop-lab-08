package it.unibo.oop.lab.mvcio;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

/**
 * 
 */
public class Controller {

    /**
     * Contains user.home.
     */
    private static final String HOME = System.getProperty("user.home");
    /**
     *  Contains default file separator.
     */
    private static final String SEPARATOR = System.getProperty("file.separator");
    /**
     * Contains the output file name.
     */
    private static final String DEFAULT_FILE = "output.txt";
    /**
     * Where the file will be saved.
     */
    private File destination = new File(HOME + SEPARATOR + DEFAULT_FILE);

    /**
     * @return the current file.
     */
    public final File getCurrentFile() {
        return destination;
    }

    /**
     * @return the file path.
     */
    public final String getPath() {
        return destination.toPath().toString();
    }


    /**
     * @param text
     *              the string to be written on file.
     * @throws IOException 
     *              if the writing fails.
     */
    public final void writeOnFile(final String text) throws IOException {
        try (PrintStream output = new PrintStream(destination)) {
            output.println(text);
        }
    }

    public final void setCurrentFile(final File newFile) {
        final File parent = newFile.getParentFile();
        if (parent.exists()) {
            System.out.println(parent.toString());
            destination = newFile;
        } else { 
            throw new IllegalArgumentException("The file cannot be saved in the new path.");
        }
    }

    public final void setCurrentFile(final String newFile) {
        setCurrentFile(new File(newFile));
    }
}
